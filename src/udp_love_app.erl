%%%-------------------------------------------------------------------
%% @doc udp_love public API
%% @end
%%%-------------------------------------------------------------------

-module(udp_love_app).

-behaviour(application).

-export([start/2, stop/1 , version/0]).

start(_StartType, _StartArgs) ->
    {ok, Port} = application:get_env(udp_love, port),
    {ok, _Cert} = application:get_env(udp_love, cert),
    {ok, _Key} = application:get_env(udp_love, key),
    {ok, Index} = application:get_env(udp_love, index),
    Dispatch = cowboy_router:compile(
                 [{'_', [{"/", cowboy_static, {priv_file, udp_love, Index}},
                         {"/token/:uuidhashed", cowboy_static, {priv_file, udp_love, "token.html"}},
                         {"/api/v0/token/:uuidhashed", udp_love_login, []}
]}]),
    {ok, _} = cowboy:start_clear(http, [{port, Port}],
                                 #{
                                   env => #{dispatch => Dispatch},
                                   stream_handlers =>
                                       [cowboy_compress_h, cowboy_stream_h]
                                  }),
    %% {ok,_Users_Pid} = udp_love_users:start_link(),
    %% {ok,_Tokens_Pid} = udp_love_tokens:start_link(),
    %% {ok,_Server_Pid} = udp_love_server:start_link(),
    udp_love_sup:start_link().

stop(_State) ->
    ok.

version() ->
    <<"0.1.0">>.

%% internal functions
