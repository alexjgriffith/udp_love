-module(udp_love_rooms).
-behaviour(gen_server).
-include_lib("stdlib/include/ms_transform.hrl").

-record(token, {token, expires, name, addr, port, gameid=false}).
-record(room, {gameid, port, pid, creator}).

-export([start_link/0,
         stop_game/1,
         new_game/1,
         member/1,
         join_game/2,
         leave_game/2,
         exists/1,
         print_table/0,
         print_game_table/1,
         tick/1,
        get_port/1]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).

%% API
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [] , []).

stop_game(GameId) ->
    [#room{gameid=GameId,pid=Pid}]=ets:lookup(?MODULE, GameId),
    gen_server:call(Pid, stop_game).

new_game(Token) ->
    gen_server:call(?MODULE, {new, Token}).

%% move to game_room
join_game(GameId, Entry) ->
    gen_server:call(?MODULE, {join, GameId, Entry}).

leave_game(GameId, Entry) ->
    gen_server:call(?MODULE, {leave, GameId, Entry}).

exists(GameId) ->
    case ets:lookup(?MODULE, GameId) of
        [Entry] -> {ok, Entry};
        [] -> {error, undefined}
    end.

get_port(GameId) ->
    #room{port=Port} = exists(GameId),
    Port.

print_table()->
    ets:foldl(fun (User, Acc) -> io:format("~p~n", [User]),Acc + 1 end, 0, ?MODULE).

print_game_table(GameId)->
    [#room{gameid=GameId,pid=Pid}]=ets:lookup(?MODULE, GameId),
    State = udp_love_game_room:get_state(Pid),
    io:format("~p~n", [State]).

member(Token) ->
    #token{token=Token, gameid=GameId} = udp_love_tokens:exists(Token),
    GameId.

tick(Pid)->
    gen_server:cast(Pid, tick).

%% Callbacks
init ([]) ->
    ?MODULE = ets:new(
                ?MODULE,
                [set, named_table, protected, {keypos,2},{read_concurrency, true}]),
    %% there has to be a better way to do this
    AvailablePorts = lists:seq(50000,50100),
    Tref=false,
    {ok, {?MODULE, AvailablePorts, Tref}}.

handle_call({new, Token},  _From, {Tid, [Port| Rest], Tref}) ->
    GameId = alphabet(8, 4),
    MatchSpec = ets:fun2ms(fun(#room{gameid=G, port=P, creator=C})
                                 when G==GameId; P==Port; C==Token ->
                                   {G,P,C}
                           end),
    case ets:select(Tid, MatchSpec) of
        [] ->
            %% Start Link Game
            {ok, Pid} = supervisor:start_child(udp_love_rooms_sup,
                                               [{GameId,Port}]),
            ets:insert_new(Tid,#room{gameid=GameId, port=Port,
                                     pid=Pid, creator=Token}),
            {reply,{ok, GameId, Port}, {Tid, Rest, Tref}};
        [{OldGame ,_,Token}| _] ->
            {reply,{ok, OldGame, Port}, {Tid, Rest, Tref}};
        [{_ ,Port,_}| _] ->
            {reply, {error, port_taken}, {Tid, Rest, Tref}};
        [{GameId, _,_}| _] ->
            {reply, {error, game_id_taken},{Tid, [Port | Rest], Tref}}
    end;

handle_call({join, GameId, Entry}, _From, {Tid, Ports, Tref}) ->
    case ets:lookup(Tid, GameId) of
        [#room{gameid=GameId,port=Port,pid=Pid}] ->
            [#room{gameid=GameId,pid=Pid}]=ets:lookup(Tid, GameId),
            Ret = udp_love_game_room:player_join(Pid,Entry),
            {reply, {Ret, Port}, {Tid, Ports, Tref}};
        _ -> {reply, {error, no_room}, {Tid, Ports, Tref}}
    end;

handle_call({leave, GameId, Entry}, _From, {Tid, Ports, Tref}) ->
    case ets:lookup(Tid, GameId) of
        [#room{gameid=GameId,pid=Pid}] ->
            ok = udp_love_game_room:player_leave(Pid,Entry),
            {reply, ok, {Tid, Ports, Tref}};
        _ ->
            {reply, ok, {Tid, Ports, Tref}}
    end;
handle_call(stop, _From, {Tid, Ports, Tref}) ->
    ets:delete(Tid),
    {stop, normal, ok, {Tid, Ports, Tref}};
handle_call(_Event, _From, Tid) ->
    {noreply, Tid}.

handle_cast(_Event,  State) ->
    {noreply, State}.

handle_info(_Event,  State) ->
    {noreply, State}.

code_change(_OldVs, State, _Extra) ->
    {ok, State}.

terminate(_Reason, _State)->
    ok.

alphabet(_Val, 0, _Human, Accu)->
    Accu;
alphabet(Val, Number, Human, Accu) ->
    Alphabet=["A","B","C","D","E","F","G","H","I","J","K","L",
              "M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"],
    Index = trunc( math:fmod(Val,26)) + 1,
    Next  = math:floor(Val / 26),
    New = list_to_binary(lists:nth(Index, Alphabet)),
    Accu1 = case {math:fmod(Number, Human) == 0 , Accu /= <<"">>} of
                {true,true} ->
                    Dash = <<"-">>,
                    <<Accu/binary, Dash/binary>>;
                _ -> Accu
            end,
    alphabet(Next, Number - 1, Human, <<Accu1/binary, New/binary>>).

alphabet(Number, Human) ->
    Val = math:floor(math:pow(26,Number + 1) * rand:uniform()),
    alphabet(Val, Number, Human, <<"">>).

%%[erlang:port_info(P) || P<-erlang:ports()].
