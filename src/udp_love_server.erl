-module(udp_love_server).
-behaviour(gen_server).

-record(token, {token, expires, name, addr, port, gameid=false}).

-export([start_link/0]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).

%% API
start_link()->
    gen_server:start_link({local,?MODULE},?MODULE,[],[]).

%% Callbacks
init ([])->
     gen_udp:open(60002, [binary,{active, true}]).

handle_call(_Event, _From, Socket) ->
    {noreply, Socket}.

handle_cast(_Event,  Socket) ->
    {noreply, Socket}.

handle_info(Event={udp, Socket,Addr,Port,Body},  Socket) ->
    {Token, Cmd, Data} = parse_message(Body),
    {Success, Entry} = udp_love_tokens:exists(Token),
    io:format("~p~p~p~n", [Token,Cmd,Data]),
    case {Success, Entry} of
        {ok, Entry} ->
            io:format("Data:~n~p~n", [Data]),
            event(Event, Cmd, Data, Entry);
        {error, token_expired} ->
            send_error(Event,Cmd,<<"token-expired">>, Token),
            io:format("Token Expired");
        {error, undefined} ->
            send_error(Event,Cmd,<<"token-undefined">>, Token),
            io:format("No Such Token")
    end,
    {noreply, Socket};
handle_info(_Event,  Socket) ->
    {noreply, Socket}.

code_change(_OldVs, Socket, _Extra) ->
    {ok, Socket}.

terminate(_Reason, _State)->
    ok.

event(Event,Cmd, Data, Entry) ->
    #token{gameid=GameId, token=Token} = Entry,
    %% Each of these comands need to be idempotent
    case Cmd of
        <<"new-game">> ->
            %% Check if player is already part of game
            case GameId of %% player doesn't get a game id until they join
                false -> %% Player is not in a game yet
                    %% Start a new game
                    {ok, NewGameId, Port} = udp_love_rooms:new_game(Token),
                    %% need to set up a "send with awks"
                    send_new_game(Event,NewGameId);
                _ ->
                    %% Check if token has created a game
                    %% if so return game code
                    %% if not make token leave any games they are in
                    %% and create a new game
                    ok = udp_love_rooms:leave_game(GameId,Token),
                    {ok, NewGameId, Port} = udp_love_rooms:new_game(Token),
                    ok = udp_love_tokens:add_game(Token,NewGameId),
                    send_new_game(Event,NewGameId)
                    %%send_error(Event,<<"in-game">>,GameId)
            end;
        <<"join-game">> ->
            {[{<<"game-id">>,GameIdJoin}]} = Data,
            io:format("game-id=~p~n",[GameIdJoin]),
            case GameId of
                GameIdJoin -> %% They are already in the right room
                    {ok,ServerPort}= udp_love_rooms:get_port(GameIdJoin),
                    send_join_game(Event,ServerPort);
                _ -> %%leave old room and join
                    case udp_love_rooms:join_game(GameIdJoin, Entry) of
                        {ok, ServerPort} ->
                            %% note that player is in game in tokens
                            case udp_love_rooms:leave_game(GameId,Entry) of
                                ok ->
                                    ok = udp_love_tokens:add_game(Token,GameIdJoin),
                                    send_join_game(Event,ServerPort);
                                {error, Error} ->
                                    send_error(Event, <<"join-game">>, Error, <<"Could Not Find Room">>)
                            end;
                            %% return the port for the specific game + room name
                        {error, max_capacity} ->
                            send_error(Event,<<"join-game">>, <<"max-capacity">>, GameId);
                        _ ->
                            send_error(Event,<<"join-game">>,<<"server-error">>, GameId)
                    end
            end;
        <<"leave-game">> ->
            case GameId of
                false -> ok;
                _ ->
                    ok = udp_love_rooms:leave_game(GameId,Token),
                    ok = udp_love_tokens:remove_game(Token)
            end,
            send_leave_game(Event);
        _ -> %% for all udp commands send a warning
            send_error(Event, <<"leave-game">>, <<"not-valid">>,
                       <<"Packet was sent to wrong port">>)

    end.

parse_message(Body) ->
    {TokenLenBin,Body1} = split_binary(Body,3),
    TokenLen = list_to_integer(binary_to_list(TokenLenBin)),
    {CmdLenBin,Body2} = split_binary(Body1,3),
    CmdLen = list_to_integer(binary_to_list(CmdLenBin)),
    {Token, Body3} = split_binary(Body2, TokenLen),
    {Cmd, Body4} = split_binary(Body3,CmdLen),
    Data = jiffy:decode(Body4),
    io:format("INCOMING:~n~p~n", [Data]),
    {Token, Cmd, Data}.

message(Type, Error, Data)->
    DataBin = jiffy:encode(Data),
    Space = <<" ">>,
    Msg = <<Type/binary, Space/binary, Error/binary, Space/binary, DataBin/binary>>,
    io:format("OUTGOING:~n~p~n", [Msg]),
    Msg.

send_error({udp, Socket,Addr,Port,_Body}, Type, Error, Description)->
    Data =  {[{<<"description">>, Description}]},
    Msg = message(Type, Error, Data),
    io:format("MSG:~p~n",[Msg]),
    gen_udp:send(Socket,Addr,Port,Msg).

send_new_game({udp, Socket,Addr,Port,_Body}, GameId)->
    Data =  {[{<<"game-id">>, GameId}]},
    Msg = message(<<"new-game">>, <<"ok">>, Data),
    gen_udp:send(Socket,Addr,Port,Msg).

send_join_game({udp, Socket,Addr,Port,_Body}, ServerPort)->
    Data =  {[{<<"port">>, ServerPort}]},
    Msg = message(<<"join-game">>, <<"ok">>, Data),
    gen_udp:send(Socket,Addr,Port,Msg).

send_leave_game({udp, Socket,Addr,Port,_Body})->
    Msg = message(<<"leave-game">>, <<"ok">>, <<"null">>),
    gen_udp:send(Socket,Addr,Port,Msg).
