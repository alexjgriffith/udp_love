-module(udp_love_users).
-behaviour(gen_server).
-include_lib("stdlib/include/ms_transform.hrl").

-export([start_link/0, stop/0, register/2, unregister/1, get_name/1, print/0]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).

-record(user, {hash, name}).

%% API
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [] , []).

stop() ->
    gen_server:call(?MODULE, stop).

register(UUID, Name) ->
    gen_server:call(?MODULE, {register, UUID, Name}).

unregister(UUID) ->
    gen_server:call(?MODULE, {unregister, UUID}).

get_name (UUID) ->
    case ets:lookup(?MODULE, UUID) of
        [{user, UUID, Name}] -> Name;
        [] -> undefined
    end.

print () ->
    ets:foldl(fun (User, Acc) ->
                      io:format("~p~n", [User]),
                      Acc + 1 end,
              0, ?MODULE).


%% Callbacks
init ([]) ->
    ?MODULE = ets:new(
                ?MODULE,
                [set, named_table, protected,{keypos,2}, {read_concurrency, true}]),
    ets:insert(?MODULE, {user, <<"Dev-Hash">>, <<"Dev">>}),
    {ok, ?MODULE}.

handle_call({register, UUID, Name},  _From, Tid) ->
    %% Make sure UUID and Name have not already been registered
    User = #user{hash=UUID, name=Name},
    MatchSpec = ets:fun2ms(fun(#user{hash=U, name=N}) when U==UUID; N==Name ->
                                   {U,N} end),
    case ets:select(Tid, MatchSpec) of
        [] ->
            ets:insert(Tid, User),
            {reply, ok, Tid};
        [{_, Name}| _] ->
            {reply, {error, name_taken}, Tid};
        [{UUID, _}| _] ->
            {reply, {error, uuid_already_exists}, Tid}
    end;
handle_call({unregister, UUID}, _From, Tid) ->
    case ets:lookup(Tid, UUID) of
        [{UUID,_Name}] ->
            %% also remove from udp_love_tokens
            ets:delete(Tid, UUID),
            {reply, ok, Tid};
        [] ->
            {reply, ok, Tid}
    end;
handle_call(stop, _From, Tid) ->
    ets:delete(Tid),
    {stop, normal, ok, Tid};
handle_call(_Event, _From, Tid) ->
    {noreply, Tid}.

handle_cast(_Event,  Tid) ->
    {noreply, Tid}.

handle_info(_Event,  Tid) ->
    {noreply, Tid}.

code_change(_OldVs, Tid, _Extra) ->
    {ok, Tid}.

terminate(_Reason, _State)->
    ok.
