%%%-------------------------------------------------------------------
%% @doc udp_love top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(udp_love_rooms_sup).

-behaviour(supervisor).

-export([start_link/0]).

-export([init/1]).

-define(SERVER, ?MODULE).

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%% sup_flags() = #{strategy => strategy(),         % optional
%%                 intensity => non_neg_integer(), % optional
%%                 period => pos_integer()}        % optional
%% child_spec() = #{id => child_id(),       % mandatory
%%                  start => mfargs(),      % mandatory
%%                  restart => restart(),   % optional
%%                  shutdown => shutdown(), % optional
%%                  type => worker(),       % optional
%%                  modules => modules()}   % optional
init([]) ->
    SupFlags = #{strategy => simple_one_for_one,
                 intensity => 5,
                 period => 10},
    ChildSpecs = [{game_room, {udp_love_game_room, start_link, []}, temporary, brutal_kill, worker, [udp_love_game_room] }],
    {ok, {SupFlags, ChildSpecs}}.

%% internal functions
