-module(udp_love_game_room).

-record(token, {token, expires, name, addr, port, gameid=false}).

-record(room_state, {gameid, socket, players=#{}, tref, globals=false, max=2}).
-record(player, {playerid, token, name, addr, port=false, time=false, state=false}).

-export([start_link/1, stop/1, player_join/2, player_leave/2, get_state/1]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).

%% API
start_link(Args)->
    io:format("~nPID~n~p~n",[self()]),
    %% this server will have no name
    gen_server:start_link(?MODULE,Args,[]).

stop(Pid)->
    gen_server:call(Pid, stop).

player_join(Pid,Entry) ->
    gen_server:call(Pid,{join,Entry}).

player_leave(Pid,Entry) ->
    gen_server:call(Pid,{leave,Entry}).

get_state(Pid) ->
    gen_server:call(Pid,{get_state}).

%% Callbacks
init ({GameId, Port})->
    io:format("~nNEW GAME ROOM~n"),
    %% will fail if port is already in use
    {ok, Socket} = gen_udp:open(Port, [binary,{active, true}]),
    %% GameId = <<"AAAA-AAAA">>,
    io:format("~nPID~n~p~n",[self()]),
    {ok, Tref} = timer:apply_interval(1000, udp_love_rooms, tick, [self()]),
    {ok, #room_state{gameid=GameId,socket=Socket, tref=Tref}}.

handle_call({get_state}, _From, State) ->
    {reply, State, State};
handle_call({join,Entry}, _From, State=#room_state{max=Max,
                                                   players=Players})->
    case maps:size(Players) >= Max of
        true -> {reply, {error,max_capacity}, State};
        false ->
            #token{token=Token}=Entry,
            Id = get_id(Players,Max),
            Player = entry_to_player (Id,Entry),
            %% maybe make an ets table?
            Players1=maps:put(Token,Player,Players),
            {reply, ok, State#room_state{players=Players1}}
    end;
handle_call({leave,Entry}, _From, State=#room_state{players=Players})->
    #token{token=Token}=Entry,
    io:format("removing:~p~n", [Token]),
    Players1=maps:remove(Token,Players),
    io:format("map:~p~n", [Players1]),
    {reply, ok, State#room_state{players=Players1}};
handle_call(stop, _From, State)->
    {stop, nomal, ok, State};
handle_call(_Event, _From, State) ->
    {noreply, State}.

handle_cast(tick,  State=#room_state{socket=Socket, players=Players}) ->
    %% io:format("TICK~n"),
    Data= maps:fold(fun (_Key,Player,{Acc}) ->
                              {[Json]} = player_to_json(Player),
                              {[Json | Acc]}
                      end,
            {[]},
            Players),
    Msg = message(<<"send-data">>, <<"ok">>, Data),
    maps:map(fun(_Key, #player{addr=Addr, port=Port}) ->
                     case Port of
                         false -> ok;%%io:format("gen_udp:send(~p, ~p, ~p, ~p)~n", [Socket, Addr, Port, Msg]);
                         _ -> gen_udp:send(Socket, Addr, Port, Msg)
                     end
             end,
             Players),
    {noreply, State};
handle_cast(_Event,  State) ->
    {noreply, State}.

handle_info({udp, Socket,Addr,Port,Body},
            State=#room_state{players=Players, gameid=GameId}) ->
    io:format("Debug:Game Room ~p Data Received ~p~n", [GameId, Body]),
    {Token, Cmd, Data} = parse_message(Body),
    Player = maps:get(Token, Players, {error, player_missing}),
    case Player of
        {error, player_missing} -> {noreply,State};
        _ ->
            Time=erlang:monotonic_time(),
            io:format("Data:~n~p~n", [Data]),
            Players2=update_player(Players, Player, Token,
                                   Data, Time, Port),
            {noreply,State#room_state{players=Players2}}
    end;
handle_info(_Event,  State) ->
    {noreply, State}.

code_change(_OldVs, State, _Extra) ->
    {ok, State}.

terminate(_Reason, #room_state{socket=Socket})->
    io:format("Closing Socket ~p~n", [Socket]),
    ok = gen_udp:close(Socket),
    ok.

%% Internal
%% move these to utils (copy in udp_love_server.erl)
parse_message(Body) ->
    {TokenLenBin,Body1} = split_binary(Body,3),
    TokenLen = list_to_integer(binary_to_list(TokenLenBin)),
    {CmdLenBin,Body2} = split_binary(Body1,3),
    CmdLen = list_to_integer(binary_to_list(CmdLenBin)),
    {Token, Body3} = split_binary(Body2, TokenLen),
    {Cmd, Body4} = split_binary(Body3,CmdLen),
    Data = jiffy:decode(Body4),
    io:format("INCOMING:~n~p~n", [Data]),
    {Token, Cmd, Data}.

message(Type, Error, Data)->
    DataBin = jiffy:encode(Data),
    Space = <<" ">>,
    Msg = <<Type/binary, Space/binary, Error/binary, Space/binary, DataBin/binary>>,
    io:format("OUTGOING:~n~p~n", [Msg]),
    Msg.


get_id(Players, Max) ->
    All = sets:from_list(lists:seq(1, Max)),
    TakenList = maps:fold(fun (_Token, #player{playerid=Id}, Acc)->
                                  [Id | Acc ] end,
                          [], Players),
    io:format("~w~n",[TakenList]),
    Taken = sets:from_list(TakenList),
    [Free|_] = sets:to_list(sets:subtract(All, Taken)),
    Free.

entry_to_player(PlayerId, #token{token=Token, name=Name, addr=Addr}) ->
    Time=erlang:monotonic_time(),
    #player{
       playerid=PlayerId,
       name = Name,
       token = Token,
       addr = Addr,
       time = Time}.

player_to_json (#player{playerid=Id, name=Name, state=State, time=Time}) ->
    {[{list_to_binary(integer_to_list(Id)),
       {[{<<"Id">>,Id},
         {<<"name">>,Name},
         {<<"Time">>,Time},
         {<<"State">>,State}]}}]}.

update_player(Players, Player, Token, Data, Time, Port) ->
    maps:put(Token,
             Player#player{state=Data, time=Time, port=Port},
             Players).
