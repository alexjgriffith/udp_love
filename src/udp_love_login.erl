-module(udp_love_login).
-behavour(cowboy_rest).

-export([init/2, terminate/3, allowed_methods/2, content_types_accepted/2, content_types_provided/2]).

-export([get_token/2, set_name/2]).

init(Req, State) -> %% Perfect for Get
    {cowboy_rest, Req, State}.

allowed_methods(Req, State) ->
    Result = [<<"GET">>, <<"HEAD">>, <<"POST">>, <<"OPTIONS">>],
    {Result, Req, State}.

content_types_accepted(Req, State) ->
    Result = [{<<"application/json">>, set_name}],
    {Result, Req, State}.

content_types_provided(Req, State) ->
    Result = [{<<"application/json">>, get_token}],
    {Result, Req, State}.

get_token (Req0, State) -> %%GET
    Hash = cowboy_req:binding(uuidhashed,Req0),
    %% io:format("Hash input~n~n~p~n~n~p",[Qs,Req0]),
    {Peer, Port} = cowboy_req:peer(Req0),
    io:format("LOGIN GET~n"),
    Name = udp_love_users:get_name(Hash),
    %% io:format("Name~n~n~p",[Name]),
    {ok, Msg} =
        case Name of
            undefined -> show_token(undefined, Name);
            _ ->
                ok = udp_love_tokens:revoke_name(Name),
                {Expires, Token} = gen_token(Hash, Peer),
                ok = udp_love_tokens:register(Token, Expires, Name,
                                              Peer, Port),
                show_token(Token, Name)
    end,
    io:format("Msg:~p~n~n",[Msg]),
    {Msg, Req0, State}.

set_name (Req0, State) -> %%POST
    io:format("LOGIN POST~n"),
    Hash = cowboy_req:binding(uuidhashed,Req0),
    {ok, Body, Req} = cowboy_req:read_body(Req0),
    Data = jiffy:decode(Body),
    {[{<<"cmd">>,<<"new-name">>},{<<"data">>,{[{<<"name">>,Name}]}}]} = Data,
    case udp_love_users:register(Hash,Name) of
        ok -> {{true, Hash}, Req, State};
        {error, name_taken} ->
            Req1 = cowboy_req:set_resp_body(
                     <<"{\"cmd\": \"error-name-taken\"}">>,
                     Req),
            {false, Req1, State};
        {error, uuid_already_exists} ->
            Req1 = cowboy_req:set_resp_body(
                     <<"{\"cmd\": \"error-uuid-already-exists\"}">>,
                    Req),
            {false, Req1, State}
    end.

terminate(_Reason, _Req, _State) ->
    ok.

gen_token(Hash, Peer) ->
    Duration = 60*60,
    Expires = erlang:monotonic_time(seconds) + Duration,
    A = list_to_binary(integer_to_list(Expires)),
    B = Hash,
    {W, X, Y, Z} = Peer,
    C = list_to_binary(integer_to_list(W)),
    D = list_to_binary(integer_to_list(X)),
    E = list_to_binary(integer_to_list(Y)),
    F = list_to_binary(integer_to_list(Z)),
    %% Form: Expiry IPV4 Hash
    Token = <<A/binary, C/binary, D/binary, E/binary, F/binary, B/binary>>,
    {Expires, base64:encode(crypto:hash(sha256, Token))}.

show_token(Token, Name) ->
    MSG = case Name of
              undefined ->
                  jiffy:encode({[{<<"cmd">>, <<"no-name">>},{<<"data">>, []}]});
              _ ->
                  jiffy:encode({[{<<"cmd">>, <<"has-name">>},
                                 {<<"data">>, {[{<<"name">>, Name},
                                               {<<"token">>, Token}
                                              ]}}]})
          end,
    %% Req = cowboy_req:reply(
    %%         200,
    %%         #{<<"content-type">> => <<"application/json">>},
    %%         MSG,
    %%         Req0),
    {ok, MSG}.
