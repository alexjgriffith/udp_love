%%%-------------------------------------------------------------------
%% @doc udp_love top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(udp_love_sup).

-behaviour(supervisor).

-export([start_link/0]).

-export([init/1]).

-define(SERVER, ?MODULE).

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%% sup_flags() = #{strategy => strategy(),         % optional
%%                 intensity => non_neg_integer(), % optional
%%                 period => pos_integer()}        % optional
%% child_spec() = #{id => child_id(),       % mandatory
%%                  start => mfargs(),      % mandatory
%%                  restart => restart(),   % optional
%%                  shutdown => shutdown(), % optional
%%                  type => worker(),       optional
%%                 modules => modules()}   % optional


init([]) ->
    _ServerSpecs = #{id => udp_love_server,
                     start => {udp_love_server, start_link, []},
                     restart => permanent,
                     shutdown => 5000,
                     type => worker,
                     modules => [udp_love_server]},
        SupFlags = #{strategy => one_for_one,
                 intensity => 5,
                 period => 10},
    ChildSpecs = [
                  {udp_love_server,{udp_love_server, start_link, []}, permanent, 5000,
                   worker, [udp_love_server]},
                  {users,{udp_love_users, start_link, []}, permanent, 5000,
                   worker, [udp_love_users]},
                  {tokens,{udp_love_tokens, start_link, []}, permanent, 5000,
                   worker, [udp_love_tokens]},
                  {rooms,{udp_love_rooms, start_link, []}, permanent, 5000,
                   worker, [udp_love_rooms]},
                 {rooms_sup,{udp_love_rooms_sup, start_link, []}, permanent, 5000,
                   supervisor, [udp_love_rooms_sup]}],
    {ok, {SupFlags, ChildSpecs}}.

%% internal functions
