-module(udp_love_tokens).
-behaviour(gen_server).
-include_lib("stdlib/include/ms_transform.hrl").

-record(token, {token, expires, name, addr, port, gameid=false}).

-export([start_link/0, stop/0, register/5, revoke/1,
         revoke_name/1, exists/1, add_game/2, remove_game/1]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).

%% API
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [] , []).

stop() ->
    gen_server:call(?MODULE, stop).

register(Token, Expires, Name, Peer, Port) ->
    Entry = make_token(Token,Expires,Name,Peer,Port),
    gen_server:call(?MODULE, {register,Entry}).

revoke(Token) ->
    gen_server:call(?MODULE, {revoke, Token}).

revoke_name(Name) ->
    gen_server:call(?MODULE, {revoke_name, Name}).

exists(Token) ->
    case ets:lookup(?MODULE, Token) of
        [Entry=#token{token=Token, expires=Expires}] ->
            Time = erlang:monotonic_time(seconds),
            case Expires > Time of
                true -> {ok, Entry};
                false ->
                    revoke(Token), %% move this to a periodic batch revoke
                    {error, token_expired}
            end;
        [] -> {error, undefined}
    end.

add_game(Token,GameId) ->
    gen_server:call(?MODULE, {add_game ,Token, GameId}).

remove_game(Token) ->
    gen_server:call(?MODULE, {remove_game, Token}).

%% Callbacks
init ([]) ->
    ?MODULE = ets:new(
                ?MODULE,
                [set, named_table, protected, {keypos, 2}, {read_concurrency, true}]),
    ets:insert(?MODULE, {token, <<"Dev-Hash">>, erlang:monotonic_time(seconds)+ 60*60*24*7,
                         <<"Dev">>, {127,0,0,1}, false, false}),
    {ok, ?MODULE}.



handle_call({register, Entry=#token{name=Name, token=Token}},  _From, Tid) ->
    %% Make sure UUID and Name have not already been registered
    MatchSpec = ets:fun2ms(fun(#token{token=T, name=N}) when N==Name; T==Token ->
                                   {T,N} end),
    case ets:select(Tid, MatchSpec) of
        [] ->
            ets:insert(Tid, Entry),
            {reply, ok, Tid};
        [{_, Name}| _] ->
            {reply, {error, name_already_exists}, Tid};
        [{Token, _}| _] ->
            {reply, {error, token_already_exists}, Tid}
    end;
handle_call({revoke, Token}, _From, Tid) ->
    case ets:lookup(Tid, Token) of
        [#token{token=Token}] ->
            ets:delete(Tid, Token),
            {reply, ok, Tid};
        [] ->
            {reply, ok, Tid}
    end;
handle_call({revoke_name, Name}, _From, Tid) ->
    MatchSpec = ets:fun2ms(fun(#token{token=T, name=N}) when N==Name ->
                                   {T,N} end),
    %% select_delte(Tab, MatchSpec) -> NumDeleted
    case ets:select(Tid, MatchSpec) of
        [{Token,_}] ->
            ets:delete(Tid, Token),
            {reply, ok, Tid};
        [First | Rest] ->
            delete_recur(Tid, First, Rest),
            {reply, ok, Tid};
        [] ->
            {reply, ok, Tid}
    end;
handle_call({add_game, Token, GameId}, _From, Tid) ->
    %% MatchSpec = ets:fun2ms(fun(Entry = #token{token=T}) when T==Token ->
    %%                                Entry#token{gameid=GameId} end),
    %% %% select_replace is atomic
    %% ets:select_replace(Tid,Matchspec),
    ets:update_element(Tid,Token,{5,GameId}),
    {reply, ok, Tid};
handle_call({remove_game, Token}, _From, Tid) ->
    %% MatchSpec = ets:fun2ms(fun(Entry = #token{token=T}) when T==Token ->
    %%                                Entry#token{gameid=false} end),
    %% %% select_replace is atomic
    %% ets:select_replace(Tid,Matchspec),
    ets:update_element(Tid,Token,{5,false}),
    {reply, ok, Tid};
handle_call(stop, _From, Tid) ->
    ets:delete(Tid),
    {stop, normal, ok, Tid};
handle_call(_Event, _From, Tid) ->
    {noreply, Tid}.

handle_cast(_Event,  Tid) ->
    {noreply, Tid}.

handle_info(_Event,  Tid) ->
    {noreply, Tid}.

code_change(_OldVs, Tid, _Extra) ->
    {ok, Tid}.

terminate(_Reason, _State)->
    ok.


%% Internal Functions
make_token(Token, Expires, Name, Addr, Port) ->
    #token{token=Token,
          expires=Expires,
          name=Name,
          addr=Addr,
          port=Port}.

delete_recur (Tid,#token{token=Token}, []) ->
    ets:delete(Tid, Token);
delete_recur (Tid,#token{token=Token}, [First | Rest]) ->
    ets:delete(Tid, Token),
    delete_recur(Tid, First, Rest).
