(local socket (require :socket))

(local json (require :lib.dkjson))
(local sha2 (require :lib.sha2))
(local lume (require :lib.lume))

(fn extra-entropy []
  (fn os.capture [cmd]
    (local f (assert (io.popen cmd :r)))
    (var s (assert (f:read :*a)))
    (f:close)
    (set s (string.gsub s "^%s+" ""))
    (set s (string.gsub s "%s+$" ""))
    (set s (string.gsub s "[\n\r]+" " "))
    s)
  (fn mac-address []
    (match (love.system.getOS)
      "Linux" (pcall os.capture "ip addr sh | grep link/ether | awk '{print $2}'")
      "Windows" (values false nil)
      "OS X" (values false nil)))
  (local (success address) (mac-address))
  (if success
      address
      "00:00:00:00:00:00"))

(fn make-uuid []
  (local uuid (require "lib.uuid")) ;; load after luaSocket
  (uuid.seed)
  (uuid.new (extra-entropy)))

(fn load-uuid []
  ;; check if file/uuid exists
  ;; if yes try to read it and return the uuid
  ;; otherwise create a new uuid using make uuid and save it to file
  (make-uuid))

;; "new-game false {\"gameid\": \"BNIM-KJWB\"}"
(fn parse [str]
  "type error data"
  (local (type error data) (str:match "^(%S*) (%S*) (.*)$"))
  (values type (if (= "ok" error) false error) (json.decode data)))

(fn pad3 [number]
  (local str (tostring number))
  (match (# str)
    3 str
    2 (.. :0 str)
    1 (.. :00 str)
    0 :000
    _ (str:sub (- (# str) 3))))

(fn message [token type data]
  "token type data"
  ;; (pp [token type data])
  (pp (.. (pad3 (# token)) (pad3  (# type)) token "" type "" (json.encode  data)))
  (.. (pad3 (# token)) (pad3 (# type)) token "" type "" (json.encode  data)))

(local client {})

(fn client.update [self dt]
  (tset self :timer (+ self.timer dt))
  ;; receive all udp messages (from server and game room)
  ;; (pp "server port read")
  (when self.server-socket
    (var (data _msg) (self.server-socket:receive))
    (while data
      (local (type error body) (parse data))
      (local await (. self.awaiting type))
      (local server-action (. self.server-actions type))
      ;; remove from awaiting and call callback
      (if await
        (do (await.callback self error body await.args)
            (tset self.awaiting type nil))
        server-action
        (server-action.callback self :server-socket body server-action.args)
        :ok)
      (set (data _msg) (self.server-socket:receive))))

  ;; (pp "game port read")
  (when self.game-socket
    (var (data _msg) (self.game-socket:receive))
    (while data
      ;; (pp data)
      (local (type error body) (parse data))
      ;; (table.insert self.received {: type : error : body})
      (pp data)
      (pp body)
      (local server-action (. self.server-actions type))
      (if (= type :send-data)
          (table.insert self.received body)
          server-action
          (server-action.callback self :game-socket body server-action.args))
      (set (data _msg) (self.game-socket:receive))))

  ;; (pp "send messeges")
  ;; send all messages, add awaiting to awaiting
  (each [_index action (ipairs self.to-send)]
    (let [socket (. self action.to)]
      (when socket
        (when action.callback
          (tset self.awaiting action.type action))
        ;; (pp action)
        (socket:send (message self.token action.type action.data)))))
  (tset self :to-send [])

  ;;(pp "increment timers")
  ;; Increment timers in awaiting and send timeouts
  (each [which await (pairs self.awaiting)]
    (tset await :timer (+ await.timer dt))
    (when (> await.timer await.timeout)
      (await.callback self :timeout nil await.args)
      (tset self.awaiting which nil)))

  ;; (pp "end if timeout")
  ;; if timer > request send all messeges in awaiting
  (when (> self.timer self.request-after)
    (each [_index action (ipairs self.awaiting)]
          (let [socket (. self action.to)]
            (when socket
              (socket:send (message self.token action.type action.data)))))
    (tset self :timer (- self.timer self.request-after))))

(fn client.loop [self dt]
  (local socket (require :socket))
  (local start (socket.gettime))
  (client.update self dt)
  ;; can sleep here
  (client.loop self (- (socket.gettime) start)))

(fn client.get-token [self callback args]
  ;; future
  ;; send request for random string
  ;; open token/hash/rstring in browser
  ;; select (update-token)
  ;; this fn recieves a callback setting the token
  ;; or the server sends a set-token command
  (love.system.openURL (.. self.tcp-address "/token/" self.hash))
  (when callback (callback self nil nil args)))

(fn client.set-token [self token]
  (pp (.. "setting token = " (or token :missing)))
  (tset self :token token))

(fn client.new-game [self callback args ?timeout]
  ;; need to set global
  ;; global needs to define all state changes / level progressions
  ;; current level is indicated in each players data
  ;; for down sheep need position of both sheep in player data
  (fn wrapper [self error body args]
    (when (not error) (tset self :game-id (. body :game-id)))
    (callback error body args))
  (local msg {:type :new-game
              :to :server-socket
              :callback wrapper
              :args args
              :data nil
              :timer 0
              :timeout (or ?timeout 5)})
  (table.insert self.to-send msg))

(fn client.leave-game [self callback args ?timeout]
  (fn wrapper [self error body args]
    (when (not error)
      (tset self :game-id nil)
      (tset self :game-socket nil)
      (tset self :game-port nil))
    (callback error body args))
  (local msg {:type :leave-game
              :to :server-socket
              :callback wrapper
              :args args
              :data nil
              :timer 0
              :timeout (or ?timeout 5)})
  (table.insert self.to-send msg))

(fn client.join-game [self gameid callback args ?timeout]
  (fn wrapper [self error body args]
    (when (not error)
      (local udp (socket.udp))
      (udp:settimeout 0.01)
      (udp:setpeername self.address body.port)
      (tset self :game-socket udp)
      (tset self :game-port body.port))
    (callback error body args))
  (local msg {:type :join-game
              :to :server-socket
              :callback wrapper
              :args args
              :data {:game-id gameid}
              :timer 0
              :timeout (or ?timeout 5)})
  (table.insert self.to-send msg))

(fn client.send-data [self data]
      (local msg {:type :send-data
                  :to :game-socket
                  :callback nil
                  :args nil
                  :data data})
  (table.insert self.to-send msg))

(fn client.has-data [self]
  (> (# self.received) 0))

(fn client.take-data [self]
  (local data (lume.clone self.received))
  (tset self :received [])
  data)

(local client-mt {
                  :__call client.update
                  :__index client})

(fn client.make-client [address port tcp-port]
  "Make a new client for udp comms with upd-love-server
address string eg \"127.0.0.1\"
port number eg 60000
tcp-port number eg 60001

example:
(local udp-love-client (require :udp-love-client))
(local client (udp-love-client.make-client :localhost 60002 60001))

(fn join-game-callback [error body _args]
  (when (not error)
    (pp body)))

(client:get-token)
(client:set-token :<<paste-token-here>>)

(client:join-game join-game-callback)
"
  (math.randomseed (socket.gettime))
  (local uuid (if (love.filesystem.isFused)
                  (load-uuid)
                  (make-uuid)))
  (local hash  (sha2.sha256 uuid))
  (local udp (socket.udp))
  (udp:settimeout 0)
  (udp:setpeername address port)
  (setmetatable {:server-socket udp
                 :server-port port
                 :tcp-address (.. "http://" address ":" tcp-port)
                 :address address
                 :game-socket nil
                 :game-port nil
                 :game-id nil
                 :name nil
                 :token nil
                 :hash hash
                 :to-send []           ;; in (to server)
                 :received []          ;; out (to game)
                 :awaiting {}          ;; client driven (new,join,leave)
                 :server-actions {}  ;; server driven (ping,kick)
                 :timer 0
                 :token nil
                 :request-after 0.1}
                client-mt)
  )

client
